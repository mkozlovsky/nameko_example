import json

from nameko.extensions import DependencyProvider
from nameko.rpc import rpc
from nameko.web.handlers import http

from common import UserNotFoundException


class UsersManager:
    def __init__(self):
        self.users = [{"id": 1}, {"id": 2}]

    def list_users(self):
        return self.users[:]
    
    def add_user(self, user_id):
        self.users.append({"id": user_id})

    def get_user(self, user_id):
        try:
            return list(filter(lambda x: x['id'] is user_id, self.users)).pop()
        except IndexError as e:
            raise  UserNotFoundException(user_id)


class UsersProvider(DependencyProvider):

    def get_dependency(self, worker_ctx):
        return self.user_manager

    def setup(self):
        self.user_manager = UsersManager()


class UsersService:
    name = "users_service"
    users_manager = UsersProvider()

    @rpc
    def list_users(self, top=10, skip=0):
        return self.users_manager.list_users()

    @rpc
    def add_user(self, user_id):
        self.users_manager.add_user(user_id)

    @rpc
    def get_user(self, user_id):
        return self.users_manager.get_user(user_id)
    
    @http('GET','/users')
    def get_users(self, request):
        return json.dumps({})

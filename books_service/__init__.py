import json
from uuid import uuid4

from nameko.extensions import DependencyProvider
from nameko.rpc import rpc, RpcProxy
from nameko.web.handlers import http


class BooksManager:
    def __init__(self):
        self.books = [{"id": str(uuid4()), "title": "Harry Potter and glory hole", "user_id": 1},
                      {"id": str(uuid4()), "title": "GOT", "user_id": 1}]

    def list_books(self):
        return self.books[:]

    def add_book(self, user_id, title):
        self.books.append({"user_id": user_id, "title": title, "id": str(uuid4())})


class BooksProvider(DependencyProvider):

    def get_dependency(self, worker_ctx):
        return self.books_manager

    def setup(self):
        self.books_manager = BooksManager()


class BooksService:
    name = "books"
    users = RpcProxy('users_service')
    books = BooksProvider()

    @rpc
    def add_book_for_user(self, user_id, book_title):
        user = self.users.get_user(user_id)
        self.books.add_book(user_id, book_title)

    @rpc
    def list_books(self):
        return self.books.list_books()

    def get_books(self, request):
        books = self.list_books()
        return json.dumps(books)

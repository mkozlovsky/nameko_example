from flask import Flask
from flask import jsonify
from flask_nameko import FlaskPooledClusterRpcProxy

rpc = FlaskPooledClusterRpcProxy()
app = Flask(__name__)
app.config.update(dict(NAMEKO_AMQP_URI="amqp://rabbitmq:rabbitmq@rabbit:5672/"))
rpc.init_app(app)


@app.route('/users/')
def list_users():
    return jsonify(rpc.users_service.list_users())


if __name__ == "__main__":
    app.run()
